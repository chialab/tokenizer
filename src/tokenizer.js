/**
 * Tokenizer
 * (c) 2017 Chialab (http://www.chialab.it) <dev@chialab.io>
 * http://chialab.io
 *
 * HTML tokenizer.
 */
import { Factory, merge, isString } from '@chialab/proteins';
import { isDiacritic } from './helpers/char-analyzer.js';
import TextPatch from './patch.js';
import Sentences from './modes/sentences.js';
import Speakings from './modes/speakings.js';
import Words from './modes/words.js';
import Letters from './modes/letters.js';
import Spaces from './modes/spaces.js';
import Punctuations from './modes/punctuations.js';
import { isNewLine  } from './helpers/node-analyzer.js';
import Parser from './helpers/parser.js';

const BLOCK_SELECTOR = [
    'p',
    'li',
    'ul',
    'div',
    'h1',
    'h2',
    'h3',
    'h4',
    'h5',
    'h6',
    'td',
    'th',
    'tr',
    'table',
    'img',
    'header',
    'article',
].join(', ');

const EXCLUDE_SELECTOR = [
    'head',
    'title',
    'meta',
    'script',
    'style',
    'img',
    'audio',
    'video',
    'object',
    'iframe',
    'svg',
    '.tokenizer--disable',
].join(', ');

/**
 * Convert HTML text to HTMLElement.
 * @private
 *
 * @param {string} text An HTML string.
 * @return {HTMLElement} The HTMLElement.
 */
function textToNode(text) {
    let parser = new Parser();
    let doc = parser.parseFromString(text, 'text/html');
    return doc.documentElement;
}

/**
 * Get a recursive list of text nodes in a Node.
 * @private
 *
 * @param {HTMLElement} node The parent to parse.
 * @return {Array} A recursive list of text nodes.
 */
function findAllTextNodes(node, options = {}, parent, textNodes, index) {
    textNodes = textNodes || [];
    index = index || 0;
    parent = parent || node;
    if (node.childNodes) {
        Array.prototype.forEach.call(node.childNodes, (child) => {
            if (child.nodeType === Parser.TEXT_NODE) {
                if (child.parentNode !== parent && !isNewLine(child)) {
                    textNodes.push(child);
                }
            } else if (child.nodeType === Parser.ELEMENT_NODE &&
                !child.matches(options.excludeSelector)) {
                findAllTextNodes(child, options, node, textNodes, index);
            }
        });
    }
    return textNodes;
}

/**
 * Split a text node in single character chunks.
 * @private
 *
 * @param {Text} node The text node to split.
 * @return {Array} A list of chunks.
 */
function splitTextNode(node, nodes) {
    let text = node.textContent;
    let doc = node.ownerDocument;
    let parent = node.parentNode;
    for (let z = 0, len = text.length; z < len; z++) {
        let nextChar = text.charAt(z + 1);
        let chunk = text.charAt(z);
        if (nextChar && isDiacritic(nextChar)) {
            z++;
            chunk += nextChar;
        }
        let tn = doc.createTextNode(chunk);
        parent.insertBefore(tn, node);
        let last = nodes[nodes.length - 1];
        if (last) {
            tn.prevToken = last;
            last.nextToken = tn;
        }
        tn.tokenizer = {};
        tn.tokenizer.index = nodes.push(tn);
    }
    parent.removeChild(node);
}

/**
 * Get a list of patches for the given node.
 * @private
 *
 * @param {Tokenizer} scope The tokenizer instance.
 * @param {HTMLElement} node The text node to analyze.
 * @return {Array} A list of Texts-es.
 */
function getPatches(scope, node, options = {}) {
    let modes = options.modes;
    let textNodes = [];
    let nodes = findAllTextNodes(node, options);
    nodes.forEach((child) => {
        splitTextNode(child, textNodes);
    });
    let patches = [];
    modes.forEach((mode) => {
        patches = patches.concat(mode(scope, node, options, textNodes));
    });
    return patches;
}

export class Tokenizer extends Factory.BaseFactory {
    get defaultConfig() {
        return {
            modes: [Letters],
            tokenTag: 't:span',
            tokenClass: 'tokenizer--token',
            tokenLetter: 'tokenizer--letter',
            tokenWord: 'tokenizer--word',
            tokenSpeaking: 'tokenizer--speaking',
            tokenSentence: 'tokenizer--sentence',
            punctuationClass: 'tokenizer--token-punctuation',
            sentenceStopClass: 'tokenizer--token-sentence-stop',
            whiteSpaceClass: 'tokenizer--token-whitespace',
            excludeSelector: EXCLUDE_SELECTOR,
            blockSelector: BLOCK_SELECTOR,
            newLineSelector: 'br',
            id: (patch, index) => index,
        };
    }
    /**
     * Tag the text.
     *
     * @param {HTMLElement|String} element The element to tag.
     * @param {Object} options Optional extra options.
     * @return {String} The tagged text.
     */
    tag(element, options = {}) {
        options = merge(this.config(), options);
        let isNode = !isString(element);
        let n = isNode ? element : textToNode(element);
        n = this.chunkNode(n, options);
        if (!isNode) {
            let html = '';
            let body = n;
            let h = n.head;
            let b = n.body;
            if (h) {
                html += h.innerHTML;
            }
            if (b) {
                body = b;
            }
            html += body.innerHTML;
            return html;
        }
        return element;
    }
    /**
     * Tag a XML node content.
     *
     * @param {HTMLElement} node The node to tag.
     * @return {string} The tagged XML content.
     */
    chunkNode(node, options = {}) {
        let patches = getPatches(this, node, options);
        patches.filter((patch) => patch.exec(options));
        patches.sort(TextPatch.sort);
        patches.forEach((patch, index) => {
            patch.id = options.id(patch, index);
        });
        return node;
    }

    getTokenData(token) {
        return token.tokenizer;
    }
}

Tokenizer.Sentences = Sentences;
Tokenizer.Speakings = Speakings;
Tokenizer.Words = Words;
Tokenizer.Letters = Letters;
Tokenizer.Spaces = Spaces;
Tokenizer.Punctuations = Punctuations;
Tokenizer.Patch = TextPatch;

export { Parser };
export * from './helpers/char-analyzer.js';
