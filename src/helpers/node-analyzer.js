import { internal } from '@chialab/proteins';
import * as CharAnalyzer from './char-analyzer.js';
import Parser from './parser.js';

function internalIsLastBlockNode(node, options) {
    if (!node.nextToken) {
        return true;
    }
    let scope = node;
    let iterateNode = node;
    let isLast = true;
    while (iterateNode) {
        if (isLast &&
            iterateNode.nextSibling &&
            iterateNode.nextSibling.matches &&
            (iterateNode.nextSibling.matches(options.newLineSelector) ||
                iterateNode.nextSibling.matches(options.excludeSelector))) {
            return true;
        }
        if (iterateNode.nodeType === Parser.ELEMENT_NODE &&
            iterateNode.matches(options.blockSelector)) {
            if (iterateNode.contains(scope.nextToken)) {
                return false;
            }
            return true;
        }
        isLast = isLast && !iterateNode.nextSibling;
        iterateNode = iterateNode.parentNode;
    }
    return false;
}

export function isLastBlockNode(node, options = {}) {
    if (!internal(node).hasOwnProperty('isLast')) {
        internal(node).isLast = internalIsLastBlockNode(node, options);
    }
    return internal(node).isLast;
}

export function isWhiteSpace(node) {
    if (!internal(node).hasOwnProperty('isWhiteSpace')) {
        internal(node).isWhiteSpace = CharAnalyzer.isWhiteSpace(node.textContent);
    }
    return internal(node).isWhiteSpace;
}

export function isNewLine(node) {
    if (!internal(node).hasOwnProperty('isNewLine')) {
        internal(node).isNewLine = !node.textContent.match(/[^\n]/) && CharAnalyzer.isNewLine(node.textContent);
    }
    return internal(node).isNewLine;
}

export function isPunctuation(node) {
    if (!internal(node).hasOwnProperty('isPunctuation')) {
        internal(node).isPunctuation = CharAnalyzer.isPunctuation(node.textContent);
    }
    return internal(node).isPunctuation;
}

export function isStopPunctuation(node) {
    if (!internal(node).hasOwnProperty('isStopPunctuation')) {
        internal(node).isStopPunctuation = CharAnalyzer.isStopPunctuation(node.textContent);
    }
    return internal(node).isStopPunctuation;
}

export function isStartPunctuation(node) {
    if (!internal(node).hasOwnProperty('isStartPunctuation')) {
        internal(node).isStartPunctuation = CharAnalyzer.isStartPunctuation(node.textContent);
    }
    return internal(node).isStartPunctuation;
}

export function isLetter(node) {
    if (!internal(node).hasOwnProperty('isLetter')) {
        internal(node).isLetter = !isWhiteSpace(node) && !isPunctuation(node);
    }
    return internal(node).isLetter;
}

export function isApostrophe(node) {
    if (!internal(node).hasOwnProperty('isApostrophe')) {
        internal(node).isApostrophe = CharAnalyzer.isApostrophe();
    }
    return internal(node).isApostrophe;
}

function getParentsNum(node) {
    let res = [];
    while (node) {
        res.push(node);
        node = node.parentNode;
    }
    return res;
}

export function isWrappable(first, last) {
    let parents1 = getParentsNum(first);
    let parents2 = getParentsNum(last);
    while (parents2.length < parents1.length) {
        let el = parents1.shift();
        if (el.previousSibling) {
            return false;
        }
    }
    return true;
}
