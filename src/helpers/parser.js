const Parser = typeof DOMParser !== 'undefined' ? DOMParser : function() {
    // eslint-disable-next-line
    const { JSDOM } = require('jsdom');
    this.parseFromString = (str) => new JSDOM(str).window.document;
};

Parser.TEXT_NODE = 3;
Parser.ELEMENT_NODE = 1;

export default Parser;
