import XRegExp from '../vendors/xregexp.js';

/**
 * A regexp for white spaces reconition.
 * @type RegExp
 */
export const WHITE_SPACES_REGEX = new RegExp('[\\s|\\n|\\r]');
/**
 * A regexp for new lines reconition.
 * @type RegExp
 */
export const NEW_LINE_REGEX = new RegExp('[\\n|\\r]');
/**
 * A regexp for punctuation reconition.
 * @type RegExp
 */
export const PUNCTUATION_REGEX = new XRegExp('[^\\p{L}|\\s|^\\d]');
/**
 * A regexp for stop punctuation reconition.
 * @type RegExp
 */
export const STOP_PUNCTUATION_REGEX = new RegExp('[.|!|?|;|·|»]');
/**
 * A regexp for start punctuation reconition.
 * @type RegExp
 */
export const START_PUNCTUATION_REGEX = new RegExp('[«|¿|¡]');
/**
 * A regexp for diacritics reconition.
 * @type RegExp
 */
export const DIACRITICS_REGEX = new XRegExp('\\p{M}');
/**
 * A regexp for a char followed by diacritic reconition.
 * @type RegExp
 */
export const FULL_DIACRITICS_REGEX = new XRegExp('.\\p{M}');
/**
 * A regexp for not alphabet chars.
 * @type RegExp
 */
export const NOT_ALPHABET_REGEX = /[^a-zA-Z]/;
/**
 * A regexp for apostrophe chars.
 * @type RegExp
 */
export const APOSTROPHE_REGEX = /[’|\']/;


/**
 * Check if char is a white space.
 * @param {String} ch The char to analyze.
 * @return {Boolean}
 */
export function isWhiteSpace(ch) {
    return WHITE_SPACES_REGEX.test(ch);
}
/**
 * Check if char is a new line.
 * @param {String} ch The char to analyze.
 * @return {Boolean}
 */
export function isNewLine(ch) {
    return NEW_LINE_REGEX.test(ch);
}
/**
 * Check if char is a punctuation char.
 * @param {String} ch The char to analyze.
 * @return {Boolean}
 */
export function isPunctuation(ch) {
    return NOT_ALPHABET_REGEX.test(ch) && PUNCTUATION_REGEX.test(ch);
}
/**
 * Check if char is a sentence stop punctuation char.
 * @param {String} ch The char to analyze.
 * @return {Boolean}
 */
export function isStopPunctuation(ch) {
    return STOP_PUNCTUATION_REGEX.test(ch);
}
/**
 * Check if char is a sentence start punctuation char.
 * @param {String} ch The char to analyze.
 * @return {Boolean}
 */
export function isStartPunctuation(ch) {
    return START_PUNCTUATION_REGEX.test(ch);
}
/**
 * Check if char is a diacritic char.
 * @param {String} ch The char to analyze.
 * @return {Boolean}
 */
export function isDiacritic(ch) {
    if (ch.length === 2) {
        return FULL_DIACRITICS_REGEX.test(ch);
    }
    return NOT_ALPHABET_REGEX.test(ch) && DIACRITICS_REGEX.test(ch);
}
/**
 * Check if char is an apostrophe.
 * @param {String} ch The char to analyze.
 * @return {Boolean}
 */
export function isApostrophe(ch) {
    return APOSTROPHE_REGEX.test(ch);
}
