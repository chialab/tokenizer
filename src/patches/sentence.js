import TextPatch from '../patch.js';
import { isFunction } from '@chialab/proteins';
import LetterPatch from './letter.js';
import WordPatch from './word.js';
import SpeakingPatch from './speaking.js';

export default class SentencePatch extends TextPatch {
    get type() {
        return 0;
    }

    constructor(...args) {
        super(...args);
        this.listenTokenData((token, key, value) => {
            switch (key) {
                case LetterPatch.dataTokenKey: {
                    let letters = this.getWrapperData('letters') || [];
                    letters.push(value);
                    this.setWrapperData('letters', letters);
                    break;
                }
                case WordPatch.dataTokenKey: {
                    let words = this.getWrapperData('words') || [];
                    words.push(value);
                    this.setWrapperData('words', words);
                    break;
                }
                case SpeakingPatch.dataTokenKey: {
                    let speakings = this.getWrapperData('speakings') || [];
                    speakings.push(value);
                    this.setWrapperData('speakings', speakings);
                    break;
                }
            }
        });
    }

    exec(options) {
        if (this.start && this.end) {
            let charClass = [options.tokenClass];
            if (options.tokenSentence) {
                if (isFunction(options.tokenSentence)) {
                    charClass.push(options.tokenSentence(this));
                } else {
                    charClass.push(options.tokenSentence);
                }
            }
            let wrapper = this.createWrapper(options.tokenTag, charClass);
            if (this.start !== this.end) {
                this.wrapElements(this.root, this.start, this.end, wrapper);
            } else {
                this.wrapElement(this.start, wrapper);
            }
            this.wrapper = wrapper;
            this.chunks.forEach((chunk) => {
                this.setTokenData(chunk, 'sentenceToken', wrapper);
            });
            return true;
        }
        return false;
    }
}
