import TextPatch from '../patch.js';
import { isFunction } from '@chialab/proteins';
import LetterPatch from './letter.js';
import WordPatch from './word.js';
import SentencePatch from './sentence.js';

export default class SpeakingPatch extends TextPatch {
    get type() {
        return 1;
    }

    constructor(...args) {
        super(...args);
        this.listenTokenData((token, key, value) => {
            switch (key) {
                case LetterPatch.dataTokenKey: {
                    let letters = this.getWrapperData('letters') || [];
                    letters.push(value);
                    this.setWrapperData('letters', letters);
                    break;
                }
                case WordPatch.dataTokenKey: {
                    let words = this.getWrapperData('words') || [];
                    words.push(value);
                    this.setWrapperData('words', words);
                    break;
                }
                case SentencePatch.dataTokenKey:
                    this.setWrapperData('sentence', value);
                    break;
            }
        });
    }

    exec(options) {
        if (this.start && this.end) {
            let charClass = [options.tokenClass];
            if (options.tokenSpeaking) {
                if (isFunction(options.tokenSpeaking)) {
                    charClass.push(options.tokenSpeaking(this));
                } else {
                    charClass.push(options.tokenSpeaking);
                }
            }
            let wrapper = this.createWrapper(options.tokenTag, charClass);
            if (this.start !== this.end) {
                this.wrapElements(this.root, this.start, this.end, wrapper);
            } else {
                this.wrapElement(this.start, wrapper);
            }
            this.wrapper = wrapper;
            this.chunks.forEach((chunk) => {
                this.setTokenData(chunk, SpeakingPatch.dataTokenKey, wrapper);
            });
            return true;
        }
        return false;
    }
}

SpeakingPatch.dataTokenKey = 'speakingToken';
