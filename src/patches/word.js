import TextPatch from '../patch.js';
import { isFunction } from '@chialab/proteins';
import LetterPatch from './letter.js';
import SpeakingPatch from './speaking.js';
import SentencePatch from './sentence.js';

export default class WordPatch extends TextPatch {
    get type() {
        return 2;
    }

    constructor(...args) {
        super(...args);
        this.listenTokenData((token, key, value) => {
            switch (key) {
                case LetterPatch.dataTokenKey: {
                    let letters = this.getWrapperData('letters') || [];
                    letters.push(value);
                    this.setWrapperData('letters', letters);
                    break;
                }
                case SpeakingPatch.dataTokenKey:
                    this.setWrapperData('speaking', value);
                    break;
                case SentencePatch.dataTokenKey:
                    this.setWrapperData('sentence', value);
                    break;
            }
        });
    }

    exec(options) {
        if (this.start && this.end) {
            let charClass = [options.tokenClass];
            if (options.tokenWord) {
                if (isFunction(options.tokenWord)) {
                    charClass.push(options.tokenWord(this));
                } else {
                    charClass.push(options.tokenWord);
                }
            }
            let wrapper = this.createWrapper(options.tokenTag, charClass);
            if (this.start !== this.end) {
                this.wrapElements(this.root, this.start, this.end, wrapper);
            } else {
                this.wrapElement(this.start, wrapper);
            }
            this.wrapper = wrapper;
            this.chunks.forEach((chunk) => {
                this.setTokenData(chunk, WordPatch.dataTokenKey, wrapper);
            });
            return true;
        }
        return false;
    }
}

WordPatch.dataTokenKey = 'wordToken';
