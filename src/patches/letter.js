import TextPatch from '../patch.js';
import { isFunction } from '@chialab/proteins';
import { isPunctuation, isStopPunctuation } from '../helpers/char-analyzer.js';
import WordPatch from './word.js';
import SpeakingPatch from './speaking.js';
import SentencePatch from './sentence.js';

export default class LetterPatch extends TextPatch {
    get type() {
        return 3;
    }

    constructor(...args) {
        super(...args);
        this.listenTokenData((token, key, value) => {
            switch (key) {
                case WordPatch.dataTokenKey:
                    this.setWrapperData('word', value);
                    break;
                case SpeakingPatch.dataTokenKey:
                    this.setWrapperData('speaking', value);
                    break;
                case SentencePatch.dataTokenKey:
                    this.setWrapperData('sentence', value);
                    break;
            }
        });
    }

    exec(options) {
        if (this.start) {
            let char = this.start.textContent;
            let isPunct = isPunctuation(char);
            let charClass = [options.tokenClass];
            if (options.tokenLetter) {
                if (isFunction(options.tokenLetter)) {
                    charClass.push(options.tokenLetter(this));
                } else {
                    charClass.push(options.tokenLetter);
                }
            }
            if (isPunct && options.punctuationClass) {
                if (isFunction(options.punctuationClass)) {
                    charClass.push(options.punctuationClass(this));
                } else {
                    charClass.push(options.punctuationClass);
                }
            }
            if (isPunct &&
                options.sentenceStopClass && isStopPunctuation(char)) {
                if (isFunction(options.sentenceStopClass)) {
                    charClass.push(options.sentenceStopClass(this));
                } else {
                    charClass.push(options.sentenceStopClass);
                }
            }
            let wrapper = this.createWrapper(options.tokenTag, charClass);
            this.wrapper = wrapper;
            this.setTokenData(this.start, LetterPatch.dataTokenKey, wrapper);
            this.wrapElement(this.start, wrapper);
            return true;
        }
        return false;
    }
}

LetterPatch.dataTokenKey = 'letterToken';
