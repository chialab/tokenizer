import SpeakingPatch from '../patches/speaking.js';
import LetterPatch from '../patches/letter.js';
import { isStartPunctuation, isLastBlockNode, isPunctuation, isLetter, isWrappable, isWhiteSpace } from '../helpers/node-analyzer.js';

export default function(scope, node, options, textNodes) {
    let desc = new SpeakingPatch(node);
    let patches = [];
    textNodes.forEach((child, index) => {
        let next = textNodes[index + 1];
        if (!desc.start) {
            if (isLetter(child) || isStartPunctuation(child)) {
                desc.setStart(child);
            } else if (isPunctuation(child)) {
                patches.push(new LetterPatch(scope, node, child));
            }
        }
        if (desc.start) {
            if (
                !next ||
                isWhiteSpace(next) ||
                isLastBlockNode(child, options) ||
                !isWrappable(desc.start, next)
            ) {
                desc.setEnd(child);
                patches.push(desc);
                desc = new SpeakingPatch(scope, node);
            } else {
                desc.addChunk(child);
            }
        }
    });
    return patches;
}
