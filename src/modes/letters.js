import LetterPatch from '../patches/letter.js';

export default function(scope, node, options, textNodes) {
    return textNodes.map((child) => new LetterPatch(scope, node, child));
}
