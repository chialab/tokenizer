import LetterPatch from '../patches/letter.js';
import { isWhiteSpace } from '../helpers/node-analyzer.js';

export default function(scope, node, options, textNodes) {
    let patches = [];
    textNodes.forEach((child) => {
        if (isWhiteSpace(child)) {
            patches.push(new LetterPatch(scope, node, child));
        }
    });
    return patches;
}
