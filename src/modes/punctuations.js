import LetterPatch from '../patches/letter.js';
import { isPunctuation } from '../helpers/node-analyzer.js';

export default function(scope, node, options, textNodes) {
    let patches = [];
    textNodes.forEach((child) => {
        if (isPunctuation(child)) {
            patches.push(new LetterPatch(scope, node, child));
        }
    });
    return patches;
}
