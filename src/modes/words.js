import WordPatch from '../patches/word.js';
import { isLastBlockNode, isApostrophe, isLetter, isWrappable } from '../helpers/node-analyzer.js';

export default function(scope, node, options, textNodes) {
    let patches = [];
    let desc = new WordPatch(scope, node);
    textNodes.forEach((child, index) => {
        let isLet = isLetter(child);
        let next = textNodes[index + 1];
        if (!desc.start && isLet) {
            desc.setStart(child);
        }
        if (desc.start) {
            if (
                (isApostrophe(child) ||
                    !next ||
                    (!isLetter(next) && !isApostrophe(next)) ||
                    isLastBlockNode(child, options)) ||
                !isWrappable(desc.start, next)) {
                desc.setEnd(child);
                patches.push(desc);
                desc = new WordPatch(scope, node);
            } else {
                desc.addChunk(child);
            }
        }
    });
    return patches;
}
