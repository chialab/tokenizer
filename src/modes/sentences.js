import SentencePatch from '../patches/sentence.js';
import { isNewLine, isStartPunctuation, isLastBlockNode, isStopPunctuation, isPunctuation, isLetter, isWrappable } from '../helpers/node-analyzer.js';

export default function(scope, node, options, textNodes) {
    let patches = [];
    let desc = new SentencePatch(scope, node);
    textNodes.forEach((child, index) => {
        let nextIndex = index + 1;
        let next = textNodes[nextIndex];
        if (!desc.start && !isNewLine(child)) {
            desc.setStart(child);
        }
        if (desc.start) {
            if (
                !next ||
                isStartPunctuation(next) ||
                isLastBlockNode(child, options) ||
                (isStopPunctuation(child) && !isPunctuation(next) && !isLetter(next)) ||
                !isWrappable(desc.start, next)
            ) {
                if (!isLastBlockNode(child, options)) {
                    while (next && isStopPunctuation(next)) {
                        nextIndex++;
                        child = next;
                        next = textNodes[nextIndex];
                    }
                }
                index = nextIndex - 1;
                desc.setEnd(child);
                patches.push(desc);
                desc = new SentencePatch(scope, node);
            } else {
                desc.addChunk(child);
            }
        }
    });

    return patches;
}
