import { NodeParents } from './helpers/node-parents.js';
import { symbolic } from '@chialab/proteins';

const SYM = symbolic('tokenizer');
const WRAPPER_SYM = symbolic('tokenizer');

export default class TextPatch {
    static sort(patch1, patch2) {
        let start1 = patch1.start;
        let start2 = patch2.start;
        if (!start2) {
            return -1;
        }
        if (!start1) {
            return 1;
        }
        let tokenizerData1 = start1.tokenizer;
        let tokenizerData2 = start2.tokenizer;
        if (tokenizerData1.index > tokenizerData2.index) {
            return 1;
        } else if (tokenizerData1.index < tokenizerData2.index) {
            return -1;
        } else if (patch1.type > patch2.type) {
            return 1;
        } else if (patch1.type < patch2.type) {
            return -1;
        }
        return 0;
    }

    constructor(tokenizer, root, start, end) {
        this.tokenizer = tokenizer;
        this.root = root;
        this.chunks = [];
        this.setStart(start);
        this.setEnd(end);
    }

    get document() {
        return this.root.ownerDocument || this.root;
    }

    /**
     * Create tagger span wrapper.
     * @private
     *
     * @param {object} options A set of tagger options.
     * @return {HTMLElement} The tagger span wrapper.
     */
    createWrapper(tokenTag, tokenClasses) {
        let elem = this.document.createElement(tokenTag);
        if (tokenClasses) {
            elem.className = tokenClasses.join(' ');
        }
        return elem;
    }

    addChunk(chunk) {
        this.chunks.push(chunk);
    }

    setStart(start) {
        this.start = start;
        if (start) {
            this.chunks.push(start);
        }
    }

    setEnd(end) {
        this.end = end;
        if (end) {
            this.chunks.push(end);
        }
    }

    exec() {
        return false;
    }

    wrapElement(node, wrapper) {
        let parent = node.parentNode;
        parent.insertBefore(wrapper, node);
        wrapper.appendChild(node);
    }

    wrapElements(root, node1, node2, wrapper) {
        let parents1 = new NodeParents(root, node1);
        let parents2 = new NodeParents(root, node2);
        let parent = parents1.findCommon(parents2);
        if (parent) {
            let children = [];
            let node = node1;
            while (node) {
                let parents = new NodeParents(root, node);
                let before = parents.getLower(parent) || node;
                if (children.indexOf(before) === -1) {
                    children.push(before);
                }
                if (node !== node2) {
                    node = node.nextToken;
                } else {
                    node = null;
                }
            }
            if (children[0]) {
                parent.insertBefore(wrapper, children[0]);
                children.forEach((child) => {
                    wrapper.appendChild(child);
                });
            }
        }
    }

    setTokenData(token, key, value) {
        if (!SYM.has(token)) {
            SYM(token, {});
        }
        let data = SYM(token);
        data[key] = value;
        return this.tokenizer.trigger('patching', this, token, key, value);
    }

    getWrapperData(key) {
        if (!WRAPPER_SYM.has(this.wrapper)) {
            WRAPPER_SYM(this.wrapper, {});
        }
        let data = WRAPPER_SYM(this.wrapper);
        return data[key];
    }

    setWrapperData(key, value) {
        if (!WRAPPER_SYM.has(this.wrapper)) {
            WRAPPER_SYM(this.wrapper, {});
        }
        let data = WRAPPER_SYM(this.wrapper);
        data[key] = value;
    }

    listenTokenData(callback) {
        this.tokenizer.on('patching', (patch, token, key, value) => {
            if (patch !== this && this.chunks.indexOf(token) !== -1) {
                callback(token, key, value);
            }
        });
    }
}

TextPatch.TOKEN_SYM = SYM;
TextPatch.WRAPPER_SYM = WRAPPER_SYM;
