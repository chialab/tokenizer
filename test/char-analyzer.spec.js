/* eslint-env mocha */
import * as CharAnalyzer from '../src/helpers/char-analyzer.js';

describe('Unit: CharAnalyzer', () => {
    describe('isWhiteSpace', () => {
        it('check real white space', () => {
            assert(CharAnalyzer.isWhiteSpace(' '));
            assert(CharAnalyzer.isWhiteSpace('\n'));
            assert(CharAnalyzer.isWhiteSpace(`
`));
        });
        it('check fake white space', () => {
            assert(!CharAnalyzer.isWhiteSpace('a'));
            assert(!CharAnalyzer.isWhiteSpace('!'));
            assert(!CharAnalyzer.isWhiteSpace('ă'));
        });
    });
    describe('isPuntuaction', () => {
        it('check real puntuaction', () => {
            assert(CharAnalyzer.isPunctuation('.'));
        });
        it('check fake puntuaction', () => {
            assert(!CharAnalyzer.isPunctuation('a'));
            assert(!CharAnalyzer.isPunctuation(' '));
        });
    });
    describe('isStopPunctuation', () => {
        it('check real puntuaction', () => {
            assert(CharAnalyzer.isPunctuation('!'));
            assert(CharAnalyzer.isStopPunctuation('!'));
        });
        it('check fake puntuaction', () => {
            assert(CharAnalyzer.isPunctuation(','));
            assert(!CharAnalyzer.isStopPunctuation(','));
            assert(!CharAnalyzer.isStopPunctuation(' '));
            assert(!CharAnalyzer.isStopPunctuation('a'));
        });
    });
    describe('isDiacritic', () => {
        it('check real diacritic', () => {
            assert(CharAnalyzer.isDiacritic('ă'));
            assert(CharAnalyzer.isDiacritic('ă'[1]));
        });
        it('check fake diacritic', () => {
            assert(!CharAnalyzer.isDiacritic('a'));
            assert(!CharAnalyzer.isDiacritic(','));
            assert(!CharAnalyzer.isDiacritic(' '));
        });
    });
});
