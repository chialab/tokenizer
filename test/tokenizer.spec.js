/* eslint-env mocha */
/* eslint-env node */
import { Tokenizer, Parser } from '../src/tokenizer.js';

const SIMPLE = 'The quick brown fox jumps over the lazy dog. Really?';
const XML = 'The <strong>quick</strong> brown <em>fox jumps</em> over the lazy dog. Really?';

function wrapper(text) {
    let parser = new Parser();
    let doc = parser.parseFromString(text, 'text/html');
    return doc;
}

describe('Unit: Tokenizer', () => {
    describe('tag', () => {
        let tagger = new Tokenizer();

        it('a simple text in letter mode', () => {
            let box = wrapper(SIMPLE);
            tagger.tag(box);
            assert.equal(
                box.querySelectorAll('.tokenizer--token').length,
                SIMPLE.length
            );
        });

        it('a simple text in word mode', () => {
            let box = wrapper(SIMPLE);
            tagger.tag(box, { modes: [Tokenizer.Words] });
            assert.equal(
                box.querySelectorAll('.tokenizer--token').length,
                SIMPLE.split(' ').length
            );
        });

        it('a simple text in word and spaces mode', () => {
            let box = wrapper(SIMPLE);
            tagger.tag(box, { modes: [Tokenizer.Words, Tokenizer.Spaces] });
            assert.equal(
                box.querySelectorAll('.tokenizer--token').length,
                SIMPLE.split(' ').length * 2 - 1
            );
        });

        it('a simple text in sentences, word and spaces mode', () => {
            let box = wrapper(SIMPLE);
            tagger.tag(box, {
                modes: [Tokenizer.Sentences, Tokenizer.Words, Tokenizer.Spaces],
            });
            assert.equal(
                box.querySelectorAll('.tokenizer--token').length,
                SIMPLE.split(' ').length * 2 + 1
            );
        });

        it('a xml text in letter mode', () => {
            let box = wrapper(XML);
            tagger.tag(box);
            assert.equal(
                box.querySelectorAll('.tokenizer--token').length,
                XML.replace(/<(?:.|\n)*?>/gm, '').length
            );
        });

        it('a xml text in word mode', () => {
            let box = wrapper(XML);
            tagger.tag(box, { modes: [Tokenizer.Words] });
            assert.equal(
                box.querySelectorAll('.tokenizer--token').length,
                XML.replace(/<(?:.|\n)*?>/gm, '').split(' ').length
            );
        });

        it('a simple text in word and letter mode', () => {
            let box = wrapper(XML);
            tagger.tag(box, { modes: [Tokenizer.Words, Tokenizer.Letters] });
            assert.equal(
                box.querySelectorAll('.tokenizer--token').length,
                SIMPLE.split(' ').length +
                SIMPLE.length
            );
        });
    });
});
